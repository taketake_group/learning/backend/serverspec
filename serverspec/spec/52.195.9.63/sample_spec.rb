# api_serverのテストコード
require 'spec_helper'

describe service('docker') do
  it { should be_enabled }
  it { should be_running }
end

describe port(8080) do
  it { should be_listening }
end

describe command("curl http://52.195.9.63:8080/hello") do
  its(:stdout) { should match 'hello' }
end
