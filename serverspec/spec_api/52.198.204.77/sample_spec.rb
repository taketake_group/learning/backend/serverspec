# 検証したいもの
# update済みか(バージョンを確認することはできるが、最新か否かのテストのやり方はわからない)
# dockerがinstallされているか、起動しているか
# dockerfileで作成した環境(APIサーバー)が正しく動いているか

require 'spec_helper'

# 内部
# テスト用
describe package('apache2')  do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# dockerが動いているか
describe package('docker')  do
  it { should be_enabled }
  it { should be_running }
end

# dockerfileが起動され、希望の環境が整っているか
# bottleサーバーの起動を確認する(curlからの返信を確認する)
describe command('curl http://52.198.204.77:8080/hello/') do
  its(:stdout) { should match /^200$/ }
end

# 外部
describe port(80) do
  it { should be_listening }
end
