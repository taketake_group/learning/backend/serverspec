require 'serverspec'
require 'net/ssh'

set :backend, :ssh

# 実行時に環境変数に "ASK_SUDO_PASSWORD"が設定されている場合
if ENV['ASK_SUDO_PASSWORD']
  begin
    require 'highline/import'
  rescue LoadError
    fail "highline is not available. Try installing it."
  end
  # コマンドライン上でパスワードの入力
  set :sudo_password, ask("Enter sudo password: ") { |q| q.echo = false }
else
  # ASK_SUDO_PASSWORD設定がない場合はここに設定したパスワードを利用
  set :sudo_password, ENV['SUDO_PASSWORD']
end

host = ENV['TARGET_HOST']

options = Net::SSH::Config.for(host)

# 接続先サーバーのユーザー名
options[:user] ||= 'ubuntu'

set :host,        options[:host_name] || host
set :ssh_options, options

# Disable sudo
# set :disable_sudo, true


# Set environment variables
# set :env, :LANG => 'C', :LC_MESSAGES => 'C'

# Set PATH
# set :path, '/sbin:/usr/local/sbin:$PATH'
